const puppeteer = require('puppeteer')

;(async () => {
	const browser = await puppeteer.launch({ headless: false })
	const page = await browser.newPage()

	for (let i = 0; i < 22; i++) {
		await page.goto('https://stg.thebizbook.com.au/register#/artist-step-2', {
			waitUntil: 'networkidle2',
		})

		await page.click('input[name="first_name"]')
		await page.keyboard.type('John')
		await page.click('input[name="last_name"]')
		await page.keyboard.type('Doe')
		await page.click('input[name="phone"]')
		await page.keyboard.type('12345678910')
		await page.click('input[name="email"]')
		await page.keyboard.type('johndoe@gmail.com')
		await page.waitForTimeout(500)
		await page.click('button[class="btn dropdown-toggle select-element"]:nth-child(1)')
		await page.click('ul[class="dropdown-menu inner"]:nth-child(1) > li:nth-child(2)')
		await page.click('input[name="zip"]')
		await page.keyboard.type('1234')

		await page.click('button[class="btn btn--pill btn--secondary btn--width btn--has-icon-on-right btn--signup-step"]')

		await page.$eval('#paypal_terms', e => e.click());

		await page.waitForTimeout(500)
		await page.click('input[name="cardholderName"]')
		await page.keyboard.type('John Doe')
		await page.click('input[name="number"]')
		await page.keyboard.type('123456789123456')
		await page.click('#payment-information > div:nth-child(1) > div.row.payment-method__wrapper > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(1) > div > div > button')
		await page.click('#payment-information > div:nth-child(1) > div.row.payment-method__wrapper > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(1) > div > div > div > ul > li:nth-child(2)')
		await page.click('#payment-information > div:nth-child(1) > div.row.payment-method__wrapper > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(2) > div > div > button')
		await page.click('#payment-information > div:nth-child(1) > div.row.payment-method__wrapper > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(2) > div > div > div > ul > li:nth-child(2)')
		await page.click('input[name="cvv"]')
		await page.keyboard.type('123')

		await page.waitForTimeout(500)
		await page.click('#payment-information > div:nth-child(4) > button')

		await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] })
	}

	await page.waitForTimeout(3000)
	await browser.close()
})()
